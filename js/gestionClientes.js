var ClientesObtenidos;

function getClientes(){
//  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers?$filter=Country eq 'Germany'"
  var url="https://services.odata.org/V4/Northwind/Northwind.svc/Customers"
  var request = new XMLHttpRequest();
  request.onreadystatechange=function() {
    if(this.readyState==4 && this.status==200) {
      console.log(request.responseText);
      ClientesObtenidos=request.responseText;
      procesarClientes();
    }
  }
  request.open("GET", url, true);
  request.send();
}

function procesarClientes(){
  var rutabandera="https://www.countries-ofthe-world.com/flags-normal/flag-of-";
  var JSONClientes=JSON.parse(ClientesObtenidos);

  var divTable=document.getElementById("divClientes");
  var tabla=document.createElement("table");

  var tbody=document.createElement("tbody");
  tabla.classList.add("table");
  tabla.classList.add("table-striped");
  for (var i=0; i<JSONClientes.value.length;i++) {
    var nuevaFila=document.createElement("tr");
    var ColumnaNombre=document.createElement("td");
    ColumnaNombre.innerText=JSONClientes.value[i].ContactName;
    nuevaFila.append(ColumnaNombre);

    var ColumnaCiudad=document.createElement("td");
    ColumnaCiudad.innerText=JSONClientes.value[i].City;
    nuevaFila.append(ColumnaCiudad);

    var ColumnaTelefono=document.createElement("td");
    ColumnaTelefono.innerText=JSONClientes.value[i].Phone;
    nuevaFila.append(ColumnaTelefono);

    var columnaBandera=document.createElement("td");
    var imgBandera=document.createElement("img");
    imgBandera.classList.add("flag");
    if (JSONClientes.value[i].Country=="UK"){
      imgBandera.src=rutabandera+"United-Kingdom.png";
    }else {
      imgBandera.src=rutabandera+JSONClientes.value[i].Country+".png";
    }
    columnaBandera.appendChild(imgBandera);
    nuevaFila.append(columnaBandera);

    tbody.appendChild(nuevaFila);
  }
  tabla.appendChild(tbody);
  divTabla.append(tabla);
}
